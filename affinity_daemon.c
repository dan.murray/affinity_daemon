#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <sched.h>

int main( int argc, char** argv ) {
	pid_t pid = fork();

	if ( pid < 0 ) {
		exit( EXIT_FAILURE );
	} else if ( pid > 0 ) {
		exit( EXIT_SUCCESS );
	}

	if ( setsid() < 0 ) {
		exit( EXIT_FAILURE );
	}

	signal( SIGCHLD, SIG_IGN );
	signal( SIGHUP, SIG_IGN );

	pid = fork();
	if ( pid > 0 ) {
		exit( EXIT_SUCCESS );
	}

	umask( 0 );
	chdir( "/" );

	for ( int i = sysconf( _SC_OPEN_MAX ); i >= 0; --i ) {
		close( i );
	}

	openlog( "affinity-daemon", LOG_PID, LOG_DAEMON );
	syslog( LOG_NOTICE, "affinity-daemon started." );

	for ( ; ; ) {
		sleep( 30 );
		for ( int i = 1; i < argc; ++i ) {
			char command[ 512 ];
			sprintf( command, "taskset -a -p ffc `pgrep %s`", argv[ i ] );
			FILE* ptaskset = popen( command, "r" );
			char line[ 512 ];
			char* pline = fgets( line, sizeof( line ), ptaskset );
			while ( pline ) {
				syslog( LOG_NOTICE, "%s", pline );
				pline = fgets( line, sizeof( line ), ptaskset );
			}
			pclose( ptaskset );
		}
	}

	syslog( LOG_NOTICE, "affinity-daemon terminated." );
	closelog();

	return EXIT_SUCCESS;
}

